import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-more-button',
  templateUrl: './more-button.component.html',
  styleUrls: ['./more-button.component.scss']
})
export class MoreButtonComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  onMoreClick(){
    this.router.navigate(['/all-projects'])
  }
}
