import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../Services/comments.service';

import { Comment }  from '../Model/comments.model';

@Component({
  selector: 'app-comments-page',
  templateUrl: './comments-page.component.html',
  styleUrls: ['./comments-page.component.scss']
})
export class CommentsPageComponent implements OnInit {

  constructor(public commentService: CommentService) { }

  @Input() gameId: string;
  comments: Comment;

  ngOnInit(): void {
    this.getComments();
  }

  getComments() {
    this.commentService.getComments(this.gameId).subscribe(data => {
      this.comments = data;
      console.log(this.comments);
    })
  }
}
