import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../Services/login.service';

import jwt_decode from "jwt-decode";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  verifyMode: boolean = false;
  verifyInfo : any = {}

  constructor(public loginService: LoginService, public router: Router) { }

  ngOnInit(): void {
  }

  loginInfoForm = new FormGroup({
      mobile: new FormControl(''),
      device_id: new FormControl('browser'),
      device_model: new FormControl('web'),
      device_os: new FormControl('angularJS')
  })

  verifyForm = new FormGroup({
    verification_code: new FormControl(''),
  })

  onSubmitClick() {
    this.loginService.sendVerificationSms(this.loginInfoForm.value).subscribe(
      response => {
          this.verifyInfo.mobile = this.loginInfoForm.value.mobile;
          this.verifyInfo.nickname = response['nickname'];
          this.verifyMode = true;
      },
      error => console.log(error.error.message)
    );
  }

  onCheckSmsCode() {
    this.verificationInfo(this.verifyForm.value.verification_code);
    this.loginService.checkVerificationSms(this.verifyInfo).subscribe(
      response => {
          localStorage.setItem('user', JSON.stringify(response));
          this.router.navigate(['/game-funders'])
      },
      error => console.log(error)
    )
  }

  verificationInfo(verification_code) {
    this.verifyInfo.verification_code = verification_code;
    this.verifyInfo.device_id = 'browser'
    this.verifyInfo.device_model = 'web'
    this.verifyInfo.device_os = 'angularJS'
  }

  decodeToken(token: any) {
    return jwt_decode(token);
  }

}
