import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameFundersComponent } from './game-funders.component';

describe('GameFundersComponent', () => {
  let component: GameFundersComponent;
  let fixture: ComponentFixture<GameFundersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameFundersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameFundersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
