import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Game } from '../Model/game.model';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdutListService {

  constructor(public http: HttpClient) { }

  getGameList(): Observable<Game[]> {
    return this.http.get<Game[]>('https://api.vasapi.click/listproducts/233?limit=6&offset=0');
  }
}
