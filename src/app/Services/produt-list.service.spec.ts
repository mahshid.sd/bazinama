import { TestBed } from '@angular/core/testing';

import { ProdutListService } from './produt-list.service';

describe('ProdutListService', () => {
  let service: ProdutListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProdutListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
