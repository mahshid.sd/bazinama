import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Game } from '../Model/game.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(public http: HttpClient) { }

  getGame(id: string): Observable<Game> {
    return this.http.get<Game>(`https://api.vasapi.click/product/${id}?device_os=ios`);
  }
}
