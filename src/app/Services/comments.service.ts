import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Comment } from '../Model/comments.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(public http: HttpClient) { }

  getComments(id: string):Observable<Comment> {
    return this.http.get<Comment>(`https://api.vasapi.click/comment/${id}`)
  }
}
