import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http: HttpClient) { }

  sendVerificationSms(phoneNumber: any) {
    return this.http.post('https://api.vasapi.click/mobile_login_step1/5', phoneNumber);
  }

  checkVerificationSms(verificationInfo: any) {
    return this.http.post('https://api.vasapi.click/mobile_login_step2/5', verificationInfo);
  }
}
