import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NavBarComponent } from './nav-bar/nav-bar.component';
import { Section1Component } from './section1/section1.component';
import { Section2Component } from './section2/section2.component';
import { GameCardComponent } from './game-card/game-card.component';
import { MoreButtonComponent } from './more-button/more-button.component';
import { Section3Component } from './section3/section3.component';
import { Section4Component } from './section4/section4.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ProjectsComponent } from './projects/projects.component';
import { LoginComponent } from './login/login.component';
import { VerificationCodeComponent } from './verification-code/verification-code.component';
import { GameFundersComponent } from './game-funders/game-funders.component';
import { QuestionsComponent } from './questions/questions.component';
import { FooterComponent } from './footer/footer.component';
import { GameDetailsComponent } from './game-details/game-details.component';
import { CommentCardComponent } from './comment-card/comment-card.component';
import { CommentsPageComponent } from './comments-page/comments-page.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    Section1Component,
    Section2Component,
    GameCardComponent,
    MoreButtonComponent,
    Section3Component,
    Section4Component,
    MainPageComponent,
    ProjectsComponent,
    LoginComponent,
    VerificationCodeComponent,
    GameFundersComponent,
    QuestionsComponent,
    FooterComponent,
    GameDetailsComponent,
    CommentsPageComponent,
    CommentCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
