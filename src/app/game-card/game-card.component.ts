import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Game } from '../Model/game.model';


@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {

  @Input() game: Game;
  percent: string;
  avatar: string;

  constructor(public router: Router) { }

  ngOnInit(): void {
    this.getPercent()
    this.getAvatar()
  }

  getPercent() {
    this.percent = this.game.invest_goal.achievement_percent + "%";
  }

  getAvatar() {
    this.avatar = 'https://api.vasapi.click/' + this.game.feature_avatar.hdpi;
  }

  onGameClick(game: Game) {
    this.router.navigate(['/game-details', game.id])
  }

}
