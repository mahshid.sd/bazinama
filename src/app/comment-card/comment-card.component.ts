import { Component, OnInit, Input } from '@angular/core';

import { Comment} from '../Model/comments.model';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.scss']
})
export class CommentCardComponent implements OnInit {

  @Input() comment: Comment;
  avatar: string;

  constructor() { }

  ngOnInit(): void {
    this.getAvatar();
    console.log(this.comment)
  }

  getAvatar() {
    this.avatar = "https://api.vasapi.click/" + this.comment.user_avatar;
    console.log(this.avatar);
  }

}
