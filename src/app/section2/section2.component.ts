import { Component, OnInit } from '@angular/core';

import { ProdutListService } from '../Services/produt-list.service';

import { Game } from '../Model/game.model';

@Component({
  selector: 'app-section2',
  templateUrl: './section2.component.html',
  styleUrls: ['./section2.component.scss']
})
export class Section2Component implements OnInit {

  constructor(public produtListService: ProdutListService) { }

  ngOnInit(): void {
    this.getGameList();
  }

  gameList :Game[];

  getGameList() {
    this.produtListService.getGameList().subscribe(data => {
      this.gameList = data;
    })
  }

}
