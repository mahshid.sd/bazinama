import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { LoginComponent } from '../login/login.component';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  nickName: string;

  constructor(public router: Router, private dialog: MatDialog) { }

  ngOnInit(): void {
    if(this.isLogedIn()){
      this.nickName = JSON.parse(localStorage.getItem('user')).nickname;
    }
  }

  onLogoClick() {
    this.router.navigate([''])
  }

  toggleMenu() {
    if(document.getElementById('dropdown-menu').className ==="hidden"){
      document.getElementById('dropdown-menu').className = "show";
    } else {
      document.getElementById('dropdown-menu').className = "hidden";
    }
  }

  toggleProfileMenu() {
    if(document.getElementById('user-popup').className ==="hidden"){
      document.getElementById('user-popup').className = "show";
    } else {
      document.getElementById('user-popup').className = "hidden";
    }
  }

  onAllProjectClick() {
    this.router.navigate(['/all-projects'])
  }

   openDialog() {
     const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '300px',
      backdropClass: 'dialog-bg-trans',
      panelClass: 'custom-modalbox'
    });
   }

   onExitClick() {
     localStorage.removeItem('user');
     document.getElementById('user-popup').className = "hidden";
   }


   isLogedIn() {
     return !!localStorage.getItem("user");
   }

   onFunderClick() {
     this.router.navigate(['game-funders']);
   }

   onQuestionClick() {
     this.router.navigate(['questions']);
   }
}
