import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductService } from '../Services/product.service';
import { Game } from '../Model/game.model';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit {

  gameId: string;
  game: Game;
  percent: string;
  avatar: string;

  constructor(private activeRoute: ActivatedRoute, public productService: ProductService) { }

  ngOnInit(): void {
    this.getGameDetails();
  }

  getGameId() {
    this.activeRoute.paramMap.subscribe(params => {
      this.gameId = params.get('id');
    })
  }

  getGameDetails() {
    this.getGameId();
    this.productService.getGame(this.gameId).subscribe( data => {
      this.game = data;
      this.getPercent();
      this.getAvatar();
    });
  }


  getPercent() {
    this.percent = this.game.invest_goal.achievement_percent + "%";
  }

  getAvatar() {
    this.avatar = 'https://api.vasapi.click/' + this.game.feature_avatar.hdpi;
  }

}
