import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPageComponent } from './main-page/main-page.component';
import { ProjectsComponent } from './projects/projects.component';
import { GameFundersComponent } from './game-funders/game-funders.component';
import { QuestionsComponent } from './questions/questions.component';
import { GameDetailsComponent } from './game-details/game-details.component';


const routes: Routes = [
  {path:'', component: MainPageComponent},
  {path:'all-projects', component: ProjectsComponent},
  {path: 'game-funders', component: GameFundersComponent},
  {path: 'questions', component: QuestionsComponent},
  {path: 'game-details/:id', component: GameDetailsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
